package com.searchmaadi.karunadu;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.DisplayMetrics;
import android.widget.RelativeLayout;

/**
 * Created by Hirak on 20-Jan-18.
 */

public class DataViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_view);
        setLeftSidebarWidth();
    }

    private void setLeftSidebarWidth() {
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        CardView leftSidebar = findViewById(R.id.left_sidebar);
        leftSidebar.getLayoutParams().width = (metrics.widthPixels/5) + 15;

        RelativeLayout topLeftBar = findViewById(R.id.top_left_bar);
        topLeftBar.getLayoutParams().width = metrics.widthPixels/5;
    }
}
