package com.searchmaadi.karunadu;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * Created by Hirak on 20-Jan-18.
 */

public class FragmentAddNewBusiness extends Fragment {

    private View view;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_add_new_business, container, false);

        Button nextBt = view.findViewById(R.id.companyDetailsNextBt);
        nextBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openExtraDetails();
            }
        });

        Button extraDetailsNextBt = view.findViewById(R.id.extraDetailsNextBt);
        extraDetailsNextBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               openConfirmDetails();
            }
        });


        return view;
    }

    private void closeFragment() {
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.remove(this);
        transaction.commit();
    }

    private void openExtraDetails(){
        view.findViewById(R.id.layout_company_details).setVisibility(View.GONE);
        view.findViewById(R.id.layout_confirm_details).setVisibility(View.GONE);
        view.findViewById(R.id.layout_extra_details).setVisibility(View.VISIBLE);

        Integers.CURRENT_SCREEN = Integers.EXTRA_DETAILS;
    }

    private void openCompanyDetails(){
        view.findViewById(R.id.layout_company_details).setVisibility(View.VISIBLE);
        view.findViewById(R.id.layout_confirm_details).setVisibility(View.GONE);
        view.findViewById(R.id.layout_extra_details).setVisibility(View.GONE);

        Integers.CURRENT_SCREEN = Integers.COMPANY_DETAILS;
    }

    private void openConfirmDetails(){
        view.findViewById(R.id.layout_company_details).setVisibility(View.GONE);
        view.findViewById(R.id.layout_confirm_details).setVisibility(View.VISIBLE);
        view.findViewById(R.id.layout_extra_details).setVisibility(View.GONE);

        Integers.CURRENT_SCREEN = Integers.CONFIRM_DETAILS;
    }
}
