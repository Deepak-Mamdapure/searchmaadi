package com.searchmaadi.karunadu;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.github.mikephil.charting.charts.HorizontalBarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private Button backBt;
    private ImageView menuIv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeViews();

        setTopBarImage();
        setLeftSidebarWidth();
        dataViewButton();
        FinancialReportButton();
        sopButton();
        attendanceButton();
        setBarChartOne();
        setBarChartTwo();
    }

    private void setBarChartTwo() {
        HorizontalBarChart barChart = findViewById(R.id.barchartToday);
        barChart.setDescription(null);    // Hide the description
        barChart.setFitBars(true); // make the x-axis fit exactly all bars

        barChart.getLegend().setEnabled(false);
        barChart.setDrawGridBackground(false);


        YAxis yAxis = barChart.getAxisLeft();
        yAxis.setDrawGridLines(false);
        yAxis.setDrawLabels(false);
        yAxis.setDrawAxisLine(false);

        yAxis = barChart.getAxisRight();
        yAxis.setDrawGridLines(false);
        yAxis.setDrawLabels(false);
        yAxis.setDrawAxisLine(false);

        XAxis xAxis = barChart.getXAxis();
        xAxis.setDrawAxisLine(true);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setAxisLineColor(ContextCompat.getColor(this, R.color.pink_graph));
        xAxis.setAxisLineWidth(3f);
        xAxis.setDrawLabels(false);
        xAxis.setDrawGridLines(false);

        List<String> labels = new ArrayList<>();
        labels.add("");
        labels.add("");
        labels.add("");

        List<BarEntry> entries = new ArrayList<>();
        entries.add(new BarEntry(0, 6));
        entries.add(new BarEntry(1, 4));
        entries.add(new BarEntry(2, 10));

        BarDataSet barDataSet = new BarDataSet(entries, "BarDataSet");


        barDataSet.setColor(ContextCompat.getColor(this, R.color.pink_graph));
        barDataSet.setDrawValues(false);
        BarData data = new BarData(barDataSet);
        data.setBarWidth(0.6f);
        barChart.setData(data);

    }

    private void setBarChartOne() {
        HorizontalBarChart barChart = findViewById(R.id.barchart);
        barChart.setDescription(null);    // Hide the description
        barChart.setFitBars(true); // make the x-axis fit exactly all bars

        barChart.getLegend().setEnabled(false);
        barChart.setDrawGridBackground(false);


        YAxis yAxis = barChart.getAxisLeft();
        yAxis.setDrawGridLines(false);
        yAxis.setDrawLabels(false);
        yAxis.setDrawAxisLine(false);

        yAxis = barChart.getAxisRight();
        yAxis.setDrawGridLines(false);
        yAxis.setDrawLabels(false);
        yAxis.setDrawAxisLine(false);

        XAxis xAxis = barChart.getXAxis();
        xAxis.setDrawAxisLine(true);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setAxisLineColor(ContextCompat.getColor(this, R.color.pink_graph));
        xAxis.setAxisLineWidth(3f);
        xAxis.setDrawLabels(false);
        xAxis.setDrawGridLines(false);

        List<String> labels = new ArrayList<>();
        labels.add("");
        labels.add("");
        labels.add("");

        List<BarEntry> entries = new ArrayList<>();
        entries.add(new BarEntry(0, 13));
        entries.add(new BarEntry(1, 10));
        entries.add(new BarEntry(2, 23));

        BarDataSet barDataSet = new BarDataSet(entries, "BarDataSet");


        barDataSet.setColor(ContextCompat.getColor(this, R.color.pink_graph));
        barDataSet.setDrawValues(false);
        BarData data = new BarData(barDataSet);
        data.setBarWidth(0.6f);
        barChart.setData(data);

    }

    private void initializeViews() {
        menuIv = findViewById(R.id.menu_iv);
        backBt = findViewById(R.id.back_bt);
        backBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Integers.CURRENT_SCREEN == Integers.CONFIRM_DETAILS) {
                    openExtraDetails();
                } else if (Integers.CURRENT_SCREEN == Integers.EXTRA_DETAILS) {
                    openCompanyDetails();
                } else if (Integers.CURRENT_SCREEN == Integers.COMPANY_DETAILS) {
                    closeFragment();
                }
            }
        });
    }

    private void closeFragment() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
        getSupportFragmentManager().beginTransaction().remove(fragment).commit();
        setSelectedTab(0);

    }

    private void openExtraDetails() {
        findViewById(R.id.layout_company_details).setVisibility(View.GONE);
        findViewById(R.id.layout_confirm_details).setVisibility(View.GONE);
        findViewById(R.id.layout_extra_details).setVisibility(View.VISIBLE);

        Integers.CURRENT_SCREEN = Integers.EXTRA_DETAILS;
    }

    private void openCompanyDetails() {
        findViewById(R.id.layout_company_details).setVisibility(View.VISIBLE);
        findViewById(R.id.layout_confirm_details).setVisibility(View.GONE);
        findViewById(R.id.layout_extra_details).setVisibility(View.GONE);

        Integers.CURRENT_SCREEN = Integers.COMPANY_DETAILS;
    }

    private void openConfirmDetails() {
        findViewById(R.id.layout_company_details).setVisibility(View.GONE);
        findViewById(R.id.layout_confirm_details).setVisibility(View.VISIBLE);
        findViewById(R.id.layout_extra_details).setVisibility(View.GONE);

        Integers.CURRENT_SCREEN = Integers.CONFIRM_DETAILS;
    }

    private void setTopBarImage() {
        ImageView topBarImage = findViewById(R.id.top_bar_iv);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT
        );

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int margin = metrics.widthPixels / 6;
        params.setMargins(margin, 0, 0, 0);
        topBarImage.setLayoutParams(params);
    }

    private void dataViewButton() {
        LinearLayout dataViewBt = findViewById(R.id.data_view_tab);
        dataViewBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSelectedTab(1);
                openAddNewBusinessFragment();
            }
        });
    }

    private void FinancialReportButton() {
        LinearLayout dataViewBt = findViewById(R.id.financial_report_tab);
        dataViewBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSelectedTab(2);
                openBlankFragment();
            }
        });
    }

    private void sopButton() {
        LinearLayout dataViewBt = findViewById(R.id.sop_tab);
        dataViewBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSelectedTab(3);
                openBlankFragment();
            }
        });
    }

    private void attendanceButton() {
        LinearLayout dataViewBt = findViewById(R.id.attendance_tab);
        dataViewBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSelectedTab(4);
                openBlankFragment();
            }
        });
    }

    private void openBlankFragment() {
        FragmentBlankBackground fragment = new FragmentBlankBackground();
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.container, fragment, "Fragment");
        transaction.commit();
        Integers.CURRENT_SCREEN = Integers.COMPANY_DETAILS;
    }

    private void openAddNewBusinessFragment() {
        FragmentAddNewBusiness fragment = new FragmentAddNewBusiness();
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.container, fragment, "Fragment");
        transaction.commit();
    }

    private void setLeftSidebarWidth() {
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        CardView leftSidebar = findViewById(R.id.left_sidebar);
        leftSidebar.getLayoutParams().width = (metrics.widthPixels / 6) + 12;

        RelativeLayout topLeftBar = findViewById(R.id.top_left_bar);
        topLeftBar.getLayoutParams().width = metrics.widthPixels / 6;
    }

    private void setSelectedTab(int position) {
        View dataTab = findViewById(R.id.selection_data_view);
        dataTab.setVisibility(View.INVISIBLE);
        View financialTab = findViewById(R.id.selection_financial_report);
        financialTab.setVisibility(View.INVISIBLE);
        View sopTab = findViewById(R.id.selection_sop);
        sopTab.setVisibility(View.INVISIBLE);
        View attendanceTab = findViewById(R.id.selection_attendance);
        attendanceTab.setVisibility(View.INVISIBLE);

        if (position == 0) {
            dataTab.setVisibility(View.INVISIBLE);
            menuIv.setVisibility(View.VISIBLE);
            backBt.setVisibility(View.INVISIBLE);
        }

        if (position == 1) {
            dataTab.setVisibility(View.VISIBLE);
            backBt.setVisibility(View.VISIBLE);
            menuIv = findViewById(R.id.menu_iv);

        } else if (position == 2) {
            financialTab.setVisibility(View.VISIBLE);
            menuIv = findViewById(R.id.menu_iv);
            backBt.setVisibility(View.VISIBLE);

        } else if (position == 3) {
            sopTab.setVisibility(View.VISIBLE);
            backBt.setVisibility(View.VISIBLE);
            menuIv = findViewById(R.id.menu_iv);

        } else if (position == 4) {
            attendanceTab.setVisibility(View.VISIBLE);
            backBt.setVisibility(View.VISIBLE);
            menuIv = findViewById(R.id.menu_iv);

        }
    }


}
